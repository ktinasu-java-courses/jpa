package org.example.template;

import lombok.RequiredArgsConstructor;
import org.example.template.exception.JpaTransactionException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

@RequiredArgsConstructor
public class JpaTransactionTemplate {
    final EntityManagerFactory emf = Persistence.createEntityManagerFactory("default");

    public <T> T executeInTransaction(final Callback<T> callback) {
        EntityManager em = null;
        EntityTransaction tx = null;

        try {
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            final T result = callback.execute(em);
            tx.commit();

            return result;
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            throw new JpaTransactionException(e);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
